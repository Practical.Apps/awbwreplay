// Wait for page (popup.html) to finish loading
document.addEventListener("DOMContentLoaded", function() {
  console.log("Popup.html loaded");
  // Pick out the fourth radio button option (Custom)
  const radio4 = document.querySelector('#option4');
  // Get the drop down elements too
  const dd1 = document.getElementById("dropdown1");
  const dd2 = document.getElementById("dropdown2");
  // Set listener for a click on that radio button
  radio4.addEventListener("click", function() {
    console.log("Radio button 4 clicked:");
    // Send request to content script requesting the list of turns from the replay selector
    browser.tabs.query({ active: true, currentWindow: true }, function (tabs) {
      browser.tabs.sendMessage(tabs[0].id, { action: "getSelectOptions" }, function (response) {
        console.log("Replay Selector Data:");
        console.log(response);

        // Expand dropdown elements' options to include the selector data
        for (let i = 0; i < response.options.length; i++) {
          const option1 = document.createElement("option");
          const option2 = document.createElement("option");

          option1.value = response.options[i].value;
          option1.text = response.options[i].text;

          option2.value = response.options[i].value;
          option2.text = response.options[i].text;

          dd1.appendChild(option1);
          dd2.appendChild(option2);
        }
      });
    });
  });
  // Set a variable for all the other elements
  const radio1 = document.querySelector('#option1');
  const radio2 = document.querySelector('#option2');
  const radio3 = document.querySelector('#option3');
  const startbutton = document.getElementById("startButton");
  // Set listener for a click on the start button
  startbutton.addEventListener("click", function() {
    console.log("Start button clicked:");
    // Create dictionary of all html form data
    const datadict = {
      1: radio1.checked,
      2: radio2.checked,
      3: radio3.checked,
      4: radio4.checked,
      5: dd1.value,
      6: dd2.value,
    };
    // Check if any of the values in the dictionary are set to true
    if (Object.values(datadict).some(value => value === true)) {
      console.log("Pre-start check: Radio button was selected");
      const num1 = parseInt(dd1.value);
      const num2 = parseInt(dd2.value);
      // Check if the custom option was selected
      if (radio4.checked === true) {
        console.log("Pre-start check: Custom setting selected");
        // If it was, check that the end turn is actually after the start turn
        if (num1 > num2) {
          // If not, do not activate the replay
          console.log("Replay not started: Start turn after End turn");
        } else {
          // If start turn is less than or equal to end turn, activate the replay
          console.log("Pre-start check: Start turn <= End turn");
          browser.tabs.query({ active: true, currentWindow: true }, function (tabs) {
            browser.tabs.sendMessage(tabs[0].id, { action: "StartReplay", datadict }, function (response) {
              console.log("Response received from StartReplay request:");
              console.log(response);
              // Close popup window because at this point the automated replay should begin
              window.close();
            });
          });
        }
      } else {
        // If any other method was selected the drop downs and their values do not matter
        console.log("Pre-start check: Predefined setting selected");
        browser.tabs.query({ active: true, currentWindow: true }, function (tabs) {
          browser.tabs.sendMessage(tabs[0].id, { action: "StartReplay", datadict }, function (response) {
            console.log("Response received from StartReplay request:");
            console.log(response);
            // Close popup window because at this point the automated replay should begin
            window.close();
          });
        });
      }
    } else {
      console.log("Replay not started: No radio button selected");
    }
    console.log(datadict)
  });
});

// Check the current active tab
browser.tabs.query({active: true, currentWindow: true}, function(tabs) {
  var activeTab = tabs[0];
  var url = activeTab.url;
  // If a game page is active, open the replay controls
  if (url.includes("awbw.amarriner.com/2030.php")) {
    console.log("On game page: " + url);
    // Send message to content script to hit the open replay button
    browser.tabs.query({ active: true, currentWindow: true }, function (tabs) {
      browser.tabs.sendMessage(tabs[0].id, { action: "OpenReplay" }, function (response) {
        console.log("Response received from OpenReplay request:");
        console.log(response);
      });
    });
  } else {
    // If the user is not on a game page, close the popup menu
    console.log("Not on game page: " + url);
    window.close();
  }
});
