let replayactive = false;

function updateIcon(tabId) {
  browser.tabs.get(tabId)
    .then(tab => {
      if (tab.url && tab.url.startsWith("https://awbw.amarriner.com/2030.php")) {
        if (!replayactive) {
          browser.browserAction.setIcon({ path: "icons/os.png", tabId: tabId });
        } else {
          cyclecheck();
        }
      } else {
        browser.browserAction.setIcon({ path: "icons/gs.png", tabId: tabId });
      }
    })
    .catch(error => {
      console.error("Error retrieving tab information:", error);
    });
}

//Listen for tab switches
browser.tabs.onActivated.addListener(activeInfo => {
  updateIcon(activeInfo.tabId);
});

// Listener for tab updates (ie changes within the same tab)
browser.tabs.onUpdated.addListener((tabId, changeInfo, tab) => {
  if (changeInfo.status === "complete") {
    if (changeInfo.url && changeInfo.url !== previousUrl) {
      updateIcon(tabId);
      previousUrl = changeInfo.url;
    }
  }
});

// Listener to receive messages from content script
browser.runtime.onMessage.addListener(function (message, sender, sendResponse) {
  if (message.target === 'background' && message.type === 'booleanValue') {
    // Access the boolean value sent from the content script
    const booleanValue = message.value;
    // Perform actions based on the boolean value
    if (booleanValue) {
      // Replay is active, start cycling the icon frames
      replayactive = true;
      cyclecheck();
      console.log('Replay Active: true');
    } else {
      // Replay has stopped, stop animation
      replayactive = false;
      console.log('Replay Active: false');
    }
  }
});

const iconFrames = ['icons/animated/frame1.png', 'icons/animated/frame2.png', 'icons/animated/frame3.png', 'icons/animated/frame4.png'];
let isCycling = false;

// Check if the icon cycling is already running and respond accordingly
function cyclecheck() {
  console.log('Cycle check activated');
  console.log(isCycling);
  // Only start cycle function once
  if (isCycling) {
    return;
  } else {
    // Set the flag to indicate that the cycling is running
    isCycling = true;
    console.log('Cycling started');
    cycleIconFrames();
  }
}

// Cycle the GE icons to show that a replay is active
function cycleIconFrames() {
  let currentIndex = 0;

  // Interval to change the icon frame every 250ms
  const intervalId = setInterval(() => {
    // Set the current frame as the addon icon
    const frame = iconFrames[currentIndex];
    browser.browserAction.setIcon({ path: frame });
    // Move to the next frame
    currentIndex = (currentIndex + 1) % iconFrames.length;
  }, 250);


  // Function to stop the cycling
  function stopCycling() {
    clearInterval(intervalId);
    // Set the icon back to OS (on game but not active)
    browser.browserAction.setIcon({ path: 'icons/os.png' });
    // Set cycling check back to false
    isCycling = false;
    console.log('Cycling stopped');
  }

  // Check the replayactive boolean
  const checkReplayActive = setInterval(() => {
    if (!replayactive) {
      stopCycling();
      clearInterval(checkReplayActive);
    }
  }, 1000);
}
