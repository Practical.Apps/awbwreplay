// Get the open replay button element
const openreplayButton = document.querySelector('.replay-open');

function openButton() {
    openreplayButton.click();
    console.log('Open Replay Button Activated');
}


// Get the replayforward button element
const forwardreplayButton = document.querySelector('.replay-forward-action');

function forwardButton() {
    forwardreplayButton.click();
    //console.log('Replay Action Forward Activated');
}


// Retreive the turn data from the replay selector
function getSelectOptions() {
  let selectField = document.querySelector(".replay-day-selector select");
  let options = selectField.options;

  const selectOptions = [];

  // Build list of options values and text labels
  for (let i = 0; i < options.length; i++) {
    selectOptions.push({
      value: options[i].value,
      text: options[i].text,
    });
  }

  return selectOptions;
}

// Check the URL index ("ndx") for the current turn
function checkTurn() {
  // Pull URL
  const url = window.location.href;
  // Set the form of the index parameter of the URL
  const turnIndexParam = "&ndx=";
  // Check if the parameter is within the URL and get position (string index)
  const startIndex = url.indexOf(turnIndexParam);
  // Define turnIndex out of the if statement scope or it cannot be returned
  let turnIndex;
  // If it is, extract string segment and parse into an integer
  if (startIndex !== -1) {
    const turnIndexStr = url.slice(startIndex + turnIndexParam.length);
    turnIndex = parseInt(turnIndexStr, 10);
  } else {
    // Otherwise return a negative integer
    const turnIndex = -1;
    console.log("Turn index parameter not found in the URL.");
  }
  // Log the turn information
  console.log("Check turn: C = " + turnIndex + ", E = " + endturn);
  // Return the current turn
  return turnIndex;
}

// Change the turn to the desired starting point
function selectTurn(turn) {
  console.log("Turn #" + turn + " selected as start");
  const dropdown = document.querySelector(".replay-day-selector select");
  // Set the value of the select element to the start turn
  dropdown.value = turn;
  // Create a custom change event, bubbling must be true for it to work
  let ce = new CustomEvent("change", {
    bubbles: true,
    cancelable: false
  });
  // Send the CE, triggering the selector.onchange function
  dropdown.dispatchEvent(ce);
  console.log("Select turn: " + turn);
}


// Start Replay
function replaystart() {
  console.log("Replay starting");
  // Set replay active flag to true so checks show the replay has started
  replayactive = true;
  // Stop ignoring mutations so that the progress function can take effect
  ignoremutations = false;
  // Start background script animation
  bganim(true);
  // Add mutation observer to game map container
  gamemapobserver.observe(targetElement, { attributes: true, childList: true, subtree: true });
  console.log("Game map observer started");
  progress();
}

// Stop Replay
function replaystop() {
  console.log("Replay was active, stopping");
  // Set replay active flag to false so checks show the replay has stopped
  replayactive = false;
  // Start ignoring mutations so that the progress will have no effect
  ignoremutations = true;
  // End background script animation
  bganim(false);
  // Disconnect game map mutation observer
  gamemapobserver.disconnect();
  console.log("Game map observer disconnected");
  // Set end turn back to -1
  endturn = -1;
}

// Send command to background script to animate icon
function bganim(booleanValue) {
  // Send the boolean value to the background script with an identifier
  browser.runtime.sendMessage({
    type: 'booleanValue',
    value: booleanValue,
    target: 'background'
  });
}

let endturn = -1;

// Automate the replay
function replay(rm, st, et) {
  const turnlist = getSelectOptions();
  console.log(turnlist);
  console.log("Replay function:");
  console.log(rm, st, et);
  // Default end turn is last possible turn
  endturn = parseInt(turnlist[turnlist.length - 1].value);
  // This will probably be redundant
  switch (rm) {
    case 1:
      console.log("Replay Starting: Everything");
      selectTurn("0");
      break;
    case 2:
      console.log("Replay Starting: Last Day");
      // Get the text attribute of the current turn (Last element)
      const currentDay = turnlist[turnlist.length - 1].text;
      // Convert to int, subtract one for previous day, back to string
      const prevDay = String(parseInt(currentDay) - 1);
      console.log("Current Day: " + currentDay + " Previous Day: " + prevDay);
      // Create lists of turns with day equal to current day and previous day
      const curDayTurns = turnlist.filter(element => element.text === currentDay);
      const prevDayTurns = turnlist.filter(element => element.text === prevDay);
      console.log(curDayTurns);
      console.log(curDayTurns.length);
      console.log(prevDayTurns);
      console.log(prevDayTurns.length);
      // Select turn on previous day equivalent to turn on current day
      selectTurn(prevDayTurns[curDayTurns.length-1].value);
      break;
    case 3:
      console.log("Replay Starting: Last Turn");
      // Last array element is current turn, 0 index means last turn is length-2
      selectTurn(String(turnlist.length-2))
      break;
    case 4:
      console.log("Replay Starting: Custom");
      endturn = parseInt(et);
      selectTurn(st);
      break;
    default:
      console.log("Replay Not Starting: Switch value is not 1, 2, 3, or 4");
  }
  // Start the replay
  replaystart();
}


//Add listener for data request from popup.js and send return message
browser.runtime.onMessage.addListener((request, sender, sendResponse) => {
  if (request.action === "OpenReplay") {
    // Add mutation observer to replay control button
    replaycontrolobserver.observe(replaycontrols, { attributes: true });
    console.log("Replay control observer started");
    // Activate the open replay button
    openButton();
    const replayopenmessage = "Replay was opened";
    sendResponse({ options: replayopenmessage });
  }
  if (request.action === "getSelectOptions") {
    const selectOptions = getSelectOptions();
    sendResponse({ options: selectOptions });
  }
  if (request.action === "StartReplay") {
    let replaystartmessage = "Replay started";
    const dd = request.datadict;
    if (dd["1"] === true) {
      replay(1, -1, -1);
      replaystartmessage += ": 1, -1, -1";
    }
    if (dd["2"] === true) {
      replay(2, -1, -1);
      replaystartmessage += ": 2, -1, -1";
    }
    if (dd["3"] === true) {
      replay(3, -1, -1);
      replaystartmessage += ": 3, -1, -1";
    }
    if (dd["4"] === true) {
      replay(4, dd["5"], dd["6"]);
      replaystartmessage += ": 4 " + ", " + dd["5"] + ", " + dd["6"];
    }
    sendResponse({ options: replaystartmessage });
  }
});


// REPLAY CONTROLS
const replaycontrols = document.querySelector('.replay-controls');
let replayactive = false;
// Visibility check first because if controls are already open the observer will not change
let rccs = window.getComputedStyle(replaycontrols);
let replaycontrolsvisible = rccs.getPropertyValue('display') !== 'none';
console.log("Replay controls already open?: " + replaycontrolsvisible);

const replaycontrolobserver = new MutationObserver((mutationsList, replaycontrolobserver) => {
  for (const mutation of mutationsList) {
    if (mutation.type === 'attributes' && mutation.attributeName === 'style') {
      // ComputedStyle necessary because the controls display is set by script
      const computedStyle = window.getComputedStyle(replaycontrols);
      // If the display style is set to none, return false. Otherwise return true
      const isVisible = computedStyle.getPropertyValue('display') !== 'none';
      if (isVisible) {
        // Replay controls are visible
        console.log("Replay Controls Visible");
        replaycontrolsvisible = true;
      } else {
        // Replay controls are not visible
        console.log("Replay Controls Hidden");
        replaycontrolsvisible = false;
        // If the replay was active and the controls are now hidden, the replay should stop
        if (replayactive) {
          replaystop();
        }
      }
    }
  }
});


// GAME MAP
const targetElement = document.querySelector('#gamemap-container');

let eventscreenvisible = false;
let ignoremutations = true;

// Define mutation observer for game map container
const gamemapobserver = new MutationObserver(mutationsList => {
  // Add observed mutations to the list
  const startTime = performance.now();
  //console.log("Mutations time:");
  //console.log(startTime);
  //console.log("Mutations list:");
  //console.log(mutationsList);
  // Check if the mutations included the event screen
  let eventscreenaffected = false;
  mutationsList.forEach(mutation => {
    if (
      mutation.type === 'attributes' &&
      mutation.attributeName === 'style' &&
      mutation.target.matches('div.event-screen')
    ) {
      const targetElement = mutation.target;
      // Check the display property of the target element
      const displayStyle = targetElement.style.display;
      if (displayStyle === 'block') {
        console.log('Event screen displayed');
        // If event screen was not visible, it has now been switched
        if (eventscreenvisible === false) {
          eventscreenaffected = true;
        }
        eventscreenvisible = true;
      } else if (displayStyle === 'none') {
        console.log('Event screen hidden');
        // If event screen was visible, it has now been switched
        if (eventscreenvisible === true) {
          eventscreenaffected = true;
        }
        eventscreenvisible = false;
      }
    }
  });
  // If mutation affected the event screen
  if (eventscreenvisible === false && eventscreenaffected === false) {
    // Event screen not affected = Normal change --> Debounce
    //console.log('This mutation will be debounced');
    processChange();
  } else if (eventscreenvisible === true && eventscreenaffected === true) {
    // Event screen affected and visibile --> Ignore changes til end
    console.log('Event screen up, start ignoring mutations');
    ignoremutations = true;
  } else if (eventscreenvisible === false && eventscreenaffected === true) {
    // Event screen affected and not visibile --> Ended, resume watching changes
    console.log('Event screen down, stop ignoring mutations and progress');
    // Check what turn it is since the turn has just changed
    let curturn = checkTurn();
    // If an end turn was been set and it is has passed, stop
    if (endturn >= 0 && curturn >= endturn) {
      console.log('End turn has been reached, stopping');
      replaystop();
    } else if (curturn < 0) {
      // If the URL no longer has the index information, stop
      console.log('The turn has been lost, stopping');
      replaystop();
    } else {
      // Otherwise, return to action progression as usual
      ignoremutations = false;
      progress();
    }
  }
});


// Function to check conditions before advancing the replay forward
function progress() {
  // If replay controls are visible and mutations are not being ignored
  if (replaycontrolsvisible && !ignoremutations) {
    // Advance the replay
    forwardButton();
    console.log('Replay progressed');
  } else {
    console.log('Replay advancement conditions not met');
  }
}

// Calling this variable triggers the debounce before the progress action
const processChange = debounce(() => progress());

// Mutation Debouncer, timer must complete before function is triggered
function debounce(func, timeout = 300){
  let timer;
  return (...args) => {
    clearTimeout(timer);
    timer = setTimeout(() => { func.apply(this, args); }, timeout);
  };
}
