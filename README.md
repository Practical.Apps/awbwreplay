# AWBWreplay

This addon will automatically cycle through AWBW game replays.

## What it does

This extension gives the user the ability to choose the extent of the replay and have the program cycle through the replay automatically.

## Why

Saves time  and effort over manual execution of replays mid-game and allows for a full replay with animations to be viewed and captured post-game.

## How

*Download*

Near the top of the repository page is a blue "Clone" button. Next to it is a symbolic download button. Either of these can be used to download the project folder.

*Install*

Since the add-on is not complete it isn't hosted by firefox it must be sideloaded. To do this visit the [about:debugging](about:debugging) page in your browser.

Click on "This Firefox" on the sidebar and then the "Load Temporary Add-on..." button

In the file browser that pops up, navigate to the (extracted) project folder and double click on any file in the main folder, I usually use `manifest.json`.

The icon for the add-on should appear on your browser toolbar, if not you will have to find it in the extensions tab, right click the icon and select "Pin to Toolbar"

*Usage*

When you click the icon for the add on, it will not show anything if you are not on an AWBW game page with a URL beggining with "https://awbw.amarriner.com/2030.php"

When you are on a game page a small window will appear with the following four options:

EVERYTHING

Starts from the very beggining

LAST DAY

Starts from the corresponding turn in the previous day of gameplay (significant for >2 player games)

LAST TURN

Starts from the last turn

CUSTOM

When you select the radio button for this option, the two dropdown menus should populate with the turn options for the game replay.

Unlike the other options which always go to the end of the replay, this option should stop at the specified turn.

*Logs*

On the [about:debugging page](about:debugging#/runtime/this-firefox) the entry for the add-on has an "Inspect" button which brings up a "Developer Tools" window.

The "Console" tab of this window shows log output from the background and popup scripts.

When you are on the game page, hitting either F12 or Ctrl+Shift+i to bring up the "Web Developer Tools" panel will show logs from the content script in the "Console" tab


## Known bugs to look out for

Turns where all of an opponent's units are in fog can cause the replay to hang, sometimes just scrolling on the page will make it resume.

Under such circumstances the event screen popup will sometimes stay up during the replay but in a small collapsed state.
